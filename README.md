Wymagania znajdują w pliku REQUIREMENTS.md

# Zalozenia
- każda stacja po stworzeniu ma 10 rowerów (dla uproszczenia, można to łatwo sparametryzować dodając parametr w klasie 'CreateStationRequest')
- preferowan są najpierw rowery, które nie są "zaparkowane" na stanowisku
- nazwa stacji jest unikalnym identyfikatorem (nie jest to zapewne docelowe rozwiązanie, ale ze względu na chęć zachowania prostoty zdecydowałem się na takie rozwiązanie. Gdybym miał trochę więcej czasu użyłbym 'UUID')
- obecnie edytować można jedynie nazwę stacji

# Komentarze
- Jeśli zależy nam na sparametryzowanym komunikacie błędu można użyć obiektu zamiast enum (ważne, żeby interface się zgadzał)
- podział na moduły: projekt jest przygotowany na rozbudowe o nowe moduły. Każdy moduł to pakiet javowy z publiczną fasadą udostępniającą wszystkie operacje, które dany moduł umie wykonać.
- pola 'public final' i 'final' vs gettery: kontrowersyjny temat, ale skłaniam się ku opini, że jeśli obiekt ma pola finalne i są to zmienne niemutowalne, to mogą być one bezpiecznie dostępne "bezpośniedio" ('public final' w przypadku DTO oraz przez dostęp pakietowy w obiektach domenowych)
- niestety przez wyjazd nie zdążyłem napisać testów integracyjnych, ale wszystkie obecne testy testują całe flow działania w pamięci i bez IO.
- obecnie mikroserwis działa "w pamięci", ale bardzo łatwo można dodać połaczenie z bazą danych/zapisywanie eventów. Wystaczy fasadzie podać inną implementację 'BicycleStationRepository'
- rowerów nie traktuje uniekalnie (liczy się tylko ich ilość)