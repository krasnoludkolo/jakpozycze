package io.github.krasnoludkolo.station.domain;

import io.github.krasnoludkolo.station.domain.api.CreateStationRequest;
import io.github.krasnoludkolo.station.domain.api.StationError;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ReturnBicycleStationFacadeTest {

    @Test
    void shouldNotReturnBicycleToNotExistingStation() {
        var stationFacade = StationFacade.inMemory();

        var result = stationFacade.returnBike("name");

        assertTrue(result.isLeft());
        assertEquals(StationError.STATION_NOT_FOUND, result.getLeft());
    }

    @Test
    void shouldReturnBikeToStation() {
        var stationFacade = StationFacade.inMemory();
        var stationName = "name";
        stationFacade.createStation(new CreateStationRequest(stationName, 1));

        stationFacade.returnBike(stationName);

        int availableBicycles = stationFacade.getStationsState().get(0).availableBicycles;
        assertEquals(BicycleStation.INITIAL_NUMBER_OF_BIKES + 1, availableBicycles);
    }

    @Test
    void shouldReturnBikeToStationToEmptyStand() {
        var stationFacade = StationFacade.inMemory();
        var stationName = "name";
        stationFacade.createStation(new CreateStationRequest(stationName, BicycleStation.INITIAL_NUMBER_OF_BIKES + 1));

        stationFacade.returnBike(stationName);

        int takenStands = stationFacade.getStationsState().get(0).takenStands;
        assertEquals(BicycleStation.INITIAL_NUMBER_OF_BIKES + 1, takenStands);
    }

    @Test
    void shouldTakenStandsNotChangeIfAllAreNotAvailable() {
        var stationFacade = StationFacade.inMemory();
        var stationName = "name";
        stationFacade.createStation(new CreateStationRequest(stationName, 1));

        stationFacade.returnBike(stationName);

        int takenStands = stationFacade.getStationsState().get(0).takenStands;
        assertEquals(1, takenStands);
    }


}