package io.github.krasnoludkolo.station.domain;

import io.github.krasnoludkolo.station.domain.api.CreateStationRequest;
import io.github.krasnoludkolo.station.domain.api.EditStationRequest;
import io.github.krasnoludkolo.station.domain.api.StationError;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class StationModifyStationFacadeTest {

    @Test
    void shouldRenameStation() {
        var stationFacade = StationFacade.inMemory();
        var oldName = "old";
        var newName = "new";
        stationFacade.createStation(new CreateStationRequest(oldName, 1));

        stationFacade.editStation(new EditStationRequest(oldName, newName));

        var actual = stationFacade.getStationsState().get(0).name;
        assertEquals(newName, actual);
    }

    @Test
    void shouldNotRenameStationWithDuplicatedName() {
        var stationFacade = StationFacade.inMemory();
        var oldName = "old";
        stationFacade.createStation(new CreateStationRequest(oldName, 1));

        var result = stationFacade.editStation(new EditStationRequest(oldName, oldName));

        assertTrue(result.isLeft());
        assertEquals(StationError.DUPLICATION_NAME, result.getLeft());
    }

    @Test
    void shouldNotRenameNotExistingStation() {
        var stationFacade = StationFacade.inMemory();

        var result = stationFacade.editStation(new EditStationRequest("old", "new"));

        assertTrue(result.isLeft());
        assertEquals(StationError.STATION_NOT_FOUND, result.getLeft());
    }

}