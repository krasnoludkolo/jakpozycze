package io.github.krasnoludkolo.station.domain;

import io.github.krasnoludkolo.station.domain.api.CreateStationRequest;
import io.github.krasnoludkolo.station.domain.api.StationError;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DeleteStationStationFacadeTest {

    @Test
    void shouldDeleteExistingStation(){
        var stationFacade = StationFacade.inMemory();
        var stationName = "test";
        stationFacade.createStation(new CreateStationRequest(stationName,1));

        stationFacade.deleteStation(stationName);

        var numberOfStation = stationFacade.getStationsState().size();
        assertEquals(0,numberOfStation);
    }

    @Test
    void shouldNotDeleteNotExistingStation(){
        var stationFacade = StationFacade.inMemory();

        var error = stationFacade.deleteStation("Not existing").getLeft();

        assertEquals(StationError.STATION_NOT_FOUND, error);
    }


}