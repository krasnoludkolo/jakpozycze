package io.github.krasnoludkolo.station.domain;

import io.github.krasnoludkolo.station.domain.api.CreateStationRequest;
import io.github.krasnoludkolo.station.domain.api.StationError;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BicycleBorrowStationFacadeTest {

    @Test
    void shouldBorrowBicycleFromStation() {
        var stationFacade = StationFacade.inMemory();
        var stationName = "test";
        stationFacade.createStation(new CreateStationRequest(stationName, 1));

        var result = stationFacade.borrowBike(stationName);

        var availableBicycles = stationFacade.getStationsState().get(0).availableBicycles;
        assertTrue(result.isRight());
        assertEquals(BicycleStation.INITIAL_NUMBER_OF_BIKES - 1, availableBicycles);
    }

    @Test
    void shouldNotBorrowBicycleFromNotExistingStation() {
        var stationFacade = StationFacade.inMemory();
        String stationName = "test";

        var result = stationFacade.borrowBike(stationName);

        assertTrue(result.isLeft());
        assertEquals(StationError.STATION_NOT_FOUND, result.getLeft());
    }

    @Test
    void shouldNotBorrowBicycleFromEmptyStation() {
        var stationFacade = StationFacade.inMemory();
        var stationName = "test";
        stationFacade.createStation(new CreateStationRequest(stationName, BicycleStation.INITIAL_NUMBER_OF_BIKES));
        borrowTenBicycles(stationName,stationFacade);

        var result = stationFacade.borrowBike(stationName);

        assertTrue(result.isLeft());
        assertEquals(StationError.NO_BICYCLE_AT_STATION, result.getLeft());
    }

    private void borrowTenBicycles(String stationName,StationFacade stationFacade) {
        for (int i = 0; i < 10; i++) {
            stationFacade.borrowBike(stationName);
        }
    }

    @Test
    void shouldBicycleStandBeEmptyAfterBorrow() {
        var stationFacade = StationFacade.inMemory();
        var stationName = "test";
        stationFacade.createStation(new CreateStationRequest(stationName, BicycleStation.INITIAL_NUMBER_OF_BIKES));

        stationFacade.borrowBike(stationName);

        int emptyStands = stationFacade.getStationsState().get(0).emptyStands;
        int takenStands = stationFacade.getStationsState().get(0).takenStands;
        assertEquals(1, emptyStands);
        assertEquals(BicycleStation.INITIAL_NUMBER_OF_BIKES - 1, takenStands);
    }

    @Test
    void shouldBorrowBicycleNotFromStandIfPossible() {
        var stationFacade = StationFacade.inMemory();
        var stationName = "test";
        int numberOfStands = 1;
        stationFacade.createStation(new CreateStationRequest(stationName, numberOfStands));

        stationFacade.borrowBike(stationName);

        int takenStands = stationFacade.getStationsState().get(0).takenStands;
        assertEquals(numberOfStands, takenStands);
    }


}