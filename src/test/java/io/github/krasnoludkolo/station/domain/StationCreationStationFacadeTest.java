package io.github.krasnoludkolo.station.domain;

import io.github.krasnoludkolo.station.domain.api.CreateStationRequest;
import io.github.krasnoludkolo.station.domain.api.StationError;
import io.github.krasnoludkolo.station.domain.api.StationStatus;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class StationCreationStationFacadeTest {

    private final String testStationName = "name";

    @Test
    void shouldReturnEmptyListWithNoStations() {
        var stationFacade = StationFacade.inMemory();

        var statusList = stationFacade.getStationsState();

        assertEquals(0, statusList.size());
    }

    @Test
    void shouldNotCreteStationWithoutName() {
        var stationFacade = StationFacade.inMemory();

        var error = stationFacade.createStation(new CreateStationRequest("", 1)).getLeft();

        assertEquals(StationError.EMPTY_NAME, error);
    }

    @Test
    void shouldCreateStationAndGetStatus() {
        var stationFacade = StationFacade.inMemory();

        stationFacade.createStation(new CreateStationRequest(testStationName, 11));
        var statusList = stationFacade.getStationsState();

        StationStatus expectedStatus = stationStatusWithTestNameAndElevenStandAndDefaultBikeNumber();
        assertEquals(1, statusList.size());
        assertEquals(expectedStatus, statusList.get(0));
    }

    @Test
    void shouldNotCreateStationWithZeroBicycleStands() {
        var stationFacade = StationFacade.inMemory();
        var createRequest = createStationRequestWithTestName(0);

        var result = stationFacade.createStation(createRequest);

        assertTrue(result.isLeft());
        assertEquals(StationError.WRONG_STANDS_NUMBER, result.getLeft());
    }

    @Test
    void shouldNotCreateStationWithNegativeNumberOfBicycleStands() {
        var stationFacade = StationFacade.inMemory();
        var createRequest = createStationRequestWithTestName(-1);

        var result = stationFacade.createStation(createRequest);

        assertTrue(result.isLeft());
        assertEquals(StationError.WRONG_STANDS_NUMBER, result.getLeft());
    }

    @Test
    void shouldNotCreateStationWithDuplicatedName() {
        var stationFacade = StationFacade.inMemory();

        stationFacade.createStation(createStationRequestWithTestName(1));
        var result = stationFacade.createStation(createStationRequestWithTestName(1));

        assertTrue(result.isLeft());
        assertEquals(StationError.DUPLICATION_NAME, result.getLeft());
    }

    @Test
    void shouldCreatedStationHasInitialNumberOfBikes() {
        var stationFacade = StationFacade.inMemory();
        var createRequest = createStationRequestWithTestName(1);

        stationFacade.createStation(createRequest);

        int expected = BicycleStation.INITIAL_NUMBER_OF_BIKES;
        int actual = stationFacade.getStationsState().get(0).availableBicycles;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCreatedActionReturnStationStatus() {
        var stationFacade = StationFacade.inMemory();
        var createRequest = createStationRequestWithTestName(11);

        var result = stationFacade.createStation(createRequest);

        var expected = stationStatusWithTestNameAndElevenStandAndDefaultBikeNumber();
        assertEquals(expected, result.get());
    }

    @Test
    void shouldReturnListOfAllStatuses() {
        var stationFacade = StationFacade.inMemory();
        var numberOfStations = 5;

        for (var i = 0; i < numberOfStations; i++) {
            stationFacade.createStation(new CreateStationRequest("test"+i,i+1));
        }

        var expected = numberOfStations;
        var actual = stationFacade.getStationsState().size();
        assertEquals(expected, actual);
    }

    private StationStatus stationStatusWithTestNameAndElevenStandAndDefaultBikeNumber() {
        return new StationStatus(testStationName, 1, 10, BicycleStation.INITIAL_NUMBER_OF_BIKES);
    }

    private CreateStationRequest createStationRequestWithTestName(int bicycleStands){
        return new CreateStationRequest(testStationName,bicycleStands);
    }
}