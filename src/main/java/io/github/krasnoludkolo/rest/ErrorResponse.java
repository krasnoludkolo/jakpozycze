package io.github.krasnoludkolo.rest;

public final class ErrorResponse {

    public final String message;

    ErrorResponse(String message) {
        this.message = message;
    }
}
