package io.github.krasnoludkolo.rest;

public interface RestResponse {

    String getMessage();
    int getHttpCode();

}
