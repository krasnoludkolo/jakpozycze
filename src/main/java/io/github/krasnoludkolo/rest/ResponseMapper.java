package io.github.krasnoludkolo.rest;

import io.vavr.control.Either;
import io.vavr.control.Option;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public final class ResponseMapper {

    private ResponseMapper() {
    }

    public static ResponseEntity map(Either<? extends RestResponse, ?> input) {
        return input
                .map(ResponseEntity::ok)
                .getOrElseGet(ResponseMapper::createErrorResponse);
    }

    private static ResponseEntity createErrorResponse(RestResponse error) {
        ErrorResponse response = new ErrorResponse(error.getMessage());
        int httpCode = error.getHttpCode();
        return new ResponseEntity<>(response, HttpStatus.valueOf(httpCode));
    }

    public static <T> ResponseEntity<T> map(Option<T> input) {
        return input
                .map(x -> new ResponseEntity<>(x, HttpStatus.OK))
                .getOrElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

}
