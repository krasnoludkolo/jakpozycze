package io.github.krasnoludkolo.station.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class StationSpringConfiguration {

    @Bean
    StationFacade stationFacade(){
        return StationFacade.inMemory();
    }


}
