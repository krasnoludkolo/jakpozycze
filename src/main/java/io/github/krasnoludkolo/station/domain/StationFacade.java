package io.github.krasnoludkolo.station.domain;

import io.github.krasnoludkolo.station.domain.api.CreateStationRequest;
import io.github.krasnoludkolo.station.domain.api.EditStationRequest;
import io.github.krasnoludkolo.station.domain.api.StationError;
import io.github.krasnoludkolo.station.domain.api.StationStatus;
import io.vavr.collection.List;
import io.vavr.control.Either;

public final class StationFacade {

    private final BicycleStationRepository repository;
    private final BicycleStationRequestValidator validator;

    static StationFacade inMemory() {
        return new StationFacade(new InMemoryBicycleStationRepository());
    }

    private StationFacade(BicycleStationRepository repository) {
        this.repository = repository;
        this.validator = new BicycleStationRequestValidator(repository);
    }

    public Either<StationError, StationStatus> borrowBike(String name) {
        return repository
                .findByName(name)
                .flatMap(BicycleStation::borrowBicycle)
                .map(repository::save)
                .map(BicycleStation::generateStatus);
    }

    public Either<StationError, StationStatus> returnBike(String name) {
        return repository
                .findByName(name)
                .flatMap(BicycleStation::returnBicycle)
                .map(repository::save)
                .map(BicycleStation::generateStatus);
    }

    public Either<StationError, StationStatus> createStation(CreateStationRequest request) {
        return validator.validate(request)
                .map(BicycleStation::create)
                .map(repository::save)
                .map(BicycleStation::generateStatus);
    }

    public Either<StationError, StationStatus> editStation(EditStationRequest request) {
        return validator.validate(request)
                .flatMap(this::editStationName);
    }

    private Either<StationError, StationStatus> editStationName(EditStationRequest request) {
        return repository.findByName(request.oldName)
                .map(station -> station.rename(request.newName))
                .map(repository::save)
                .map(BicycleStation::generateStatus);
    }

    public Either<StationError, Void> deleteStation(String stationName) {
        var deleted = repository.deleteByName(stationName);
        return deleted ? Either.right(null) : Either.left(StationError.STATION_NOT_FOUND);
    }

    public List<StationStatus> getStationsState() {
        return repository.findAll().map(BicycleStation::generateStatus);
    }

}
