package io.github.krasnoludkolo.station.domain;

import io.github.krasnoludkolo.station.domain.api.CreateStationRequest;
import io.github.krasnoludkolo.station.domain.api.EditStationRequest;
import io.github.krasnoludkolo.station.domain.api.StationError;
import io.vavr.control.Either;

final class BicycleStationRequestValidator {

    private final BicycleStationRepository repository;

    BicycleStationRequestValidator(BicycleStationRepository repository) {
        this.repository = repository;
    }

    Either<StationError, CreateStationRequest> validate(CreateStationRequest request){
        if(request.bicycleStands < 1){
            return Either.left(StationError.WRONG_STANDS_NUMBER);
        }else if(repository.exists(request.name)){
            return Either.left(StationError.DUPLICATION_NAME);
        }else if(request.name.isEmpty()){
            return Either.left(StationError.EMPTY_NAME);
        }
        return Either.right(request);
    }

    Either<StationError, EditStationRequest> validate(EditStationRequest request){
        if(repository.exists(request.newName)){
            return Either.left(StationError.DUPLICATION_NAME);
        }
        return Either.right(request);
    }


}
