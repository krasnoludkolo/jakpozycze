package io.github.krasnoludkolo.station.domain.api;

import io.github.krasnoludkolo.rest.RestResponse;

public enum StationError implements RestResponse {
    STATION_NOT_FOUND("Station not found",404),
    WRONG_STANDS_NUMBER("Number of bicycle stands must be greater then 0",400),
    EMPTY_NAME("Name can not be empty",400),
    NO_BICYCLE_AT_STATION("Station has no available bicycles",400),
    DUPLICATION_NAME("Name already taken",400);


    private String message;
    private int code;

    StationError(String message, int code) {
        this.message = message;
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public int getHttpCode() {
        return code;
    }
}
