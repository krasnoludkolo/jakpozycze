package io.github.krasnoludkolo.station.domain.api;

import java.beans.ConstructorProperties;

public final class CreateStationRequest {

    public final String name;
    public final int bicycleStands;

    @ConstructorProperties({"name","bicycleStands"})
    public CreateStationRequest(String name, int bicycleStands) {
        this.name = name;
        this.bicycleStands = bicycleStands;
    }
}
