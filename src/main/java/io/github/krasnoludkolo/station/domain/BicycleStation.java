package io.github.krasnoludkolo.station.domain;

import io.github.krasnoludkolo.station.domain.api.CreateStationRequest;
import io.github.krasnoludkolo.station.domain.api.StationError;
import io.github.krasnoludkolo.station.domain.api.StationStatus;
import io.vavr.collection.List;
import io.vavr.control.Either;

final class BicycleStation {

    public static final int INITIAL_NUMBER_OF_BIKES = 10;

    final String name;
    final List<BicycleStand> stands;
    final int numberOfBikes;

    static BicycleStation create(CreateStationRequest request) {
        var name = request.name;
        var takenStands = List
                .range(0, numberOfTakenStands(request.bicycleStands))
                .map(i -> BicycleStand.taken());
        var emptyStands = List
                .range(0, numberOfEmptyStands(request.bicycleStands))
                .map(i -> BicycleStand.empty());
        return new BicycleStation(name, takenStands.appendAll(emptyStands), INITIAL_NUMBER_OF_BIKES);
    }

    private static int numberOfTakenStands(int bicycleStands) {
        return Math.min(bicycleStands, INITIAL_NUMBER_OF_BIKES);
    }

    private static int numberOfEmptyStands(int bicycleStands) {
        return Math.max(0, bicycleStands - INITIAL_NUMBER_OF_BIKES);
    }

    private BicycleStation(String name, List<BicycleStand> stands, int numberOfBikes) {
        this.name = name;
        this.stands = stands;
        this.numberOfBikes = numberOfBikes;
    }

    BicycleStation rename(String newName) {
        return new BicycleStation(newName, stands, numberOfBikes);
    }

    Either<StationError, BicycleStation> borrowBicycle() {
        if (noAvailableBicycles()) {
            return Either.left(StationError.NO_BICYCLE_AT_STATION);
        } else {
            return Either.right(borrowAvailableBicycle());
        }
    }

    private BicycleStation borrowAvailableBicycle() {
        int bicyclesAfterBorrow = this.numberOfBikes - 1;
        if (moreBicyclesThenStands()) {
            return new BicycleStation(name, stands, bicyclesAfterBorrow);
        } else {
            List<BicycleStand> standsAfterBorrow = toggleFirstIfPossible(stands, false);
            return new BicycleStation(name, standsAfterBorrow, bicyclesAfterBorrow);
        }
    }

    private boolean moreBicyclesThenStands() {
        return numberOfBikes > stands.filterNot(BicycleStand::isEmpty).size();
    }

    private boolean noAvailableBicycles() {
        return numberOfBikes < 1;
    }

    Either<StationError, BicycleStation> returnBicycle() {
        List<BicycleStand> standsAfterReturn = toggleFirstIfPossible(stands, true);
        return Either.right(new BicycleStation(name, standsAfterReturn, numberOfBikes + 1));
    }

    private List<BicycleStand> toggleFirstIfPossible(List<BicycleStand> list, boolean empty) {
        return list
                .filter(stand -> stand.isEmpty == empty)
                .headOption()
                .map(head -> list.remove(head).append(head.toggle()))
                .getOrElse(list);
    }

    StationStatus generateStatus() {
        var emptyStands = stands.filter(BicycleStand::isEmpty).size();
        var takenStands = stands.filterNot(BicycleStand::isEmpty).size();
        return new StationStatus(name, emptyStands, takenStands, numberOfBikes);
    }


}
