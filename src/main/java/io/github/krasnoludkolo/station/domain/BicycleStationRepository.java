package io.github.krasnoludkolo.station.domain;

import io.github.krasnoludkolo.station.domain.api.StationError;
import io.vavr.collection.List;
import io.vavr.control.Either;

interface BicycleStationRepository {

    BicycleStation save(BicycleStation station);
    boolean deleteByName(String name);
    boolean exists(String name);
    List<BicycleStation> findAll();
    Either<StationError,BicycleStation> findByName(String name);

}
