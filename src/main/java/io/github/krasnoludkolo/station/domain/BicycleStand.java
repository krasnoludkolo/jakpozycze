package io.github.krasnoludkolo.station.domain;

final class BicycleStand {

    final boolean isEmpty;

    static BicycleStand empty(){
        return new BicycleStand(true);
    }

    static BicycleStand taken(){
        return new BicycleStand(false);
    }

    private BicycleStand(boolean isEmpty) {
        this.isEmpty = isEmpty;
    }

    boolean isEmpty(){
        return isEmpty;
    }

    BicycleStand toggle(){
        return new BicycleStand(!isEmpty);
    }
}
