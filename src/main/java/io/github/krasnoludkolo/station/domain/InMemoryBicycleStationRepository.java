package io.github.krasnoludkolo.station.domain;

import io.github.krasnoludkolo.station.domain.api.StationError;
import io.vavr.collection.List;
import io.vavr.control.Either;
import io.vavr.control.Option;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

final class InMemoryBicycleStationRepository implements BicycleStationRepository {

    private Map<String, BicycleStation> database = new ConcurrentHashMap<>();

    @Override
    public BicycleStation save(BicycleStation station) {
        database.put(station.name, station);
        return station;
    }

    @Override
    public boolean deleteByName(String name) {
        return database.remove(name) != null;
    }

    @Override
    public boolean exists(String name) {
        return database.containsKey(name);
    }

    @Override
    public List<BicycleStation> findAll() {
        return List.ofAll(database.values());
    }

    @Override
    public Either<StationError, BicycleStation> findByName(String name) {
        return Option.of(database.get(name)).toEither(StationError.STATION_NOT_FOUND);
    }
}
