package io.github.krasnoludkolo.station.domain.api;

import java.beans.ConstructorProperties;

public final class EditStationRequest {

    public final String oldName;
    public final String newName;

    @ConstructorProperties({"oldName","newName"})
    public EditStationRequest(String oldName, String newName) {
        this.oldName = oldName;
        this.newName = newName;
    }

}
