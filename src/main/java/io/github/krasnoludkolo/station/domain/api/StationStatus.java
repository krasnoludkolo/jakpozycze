package io.github.krasnoludkolo.station.domain.api;

import java.util.Objects;

public final class StationStatus {

    public final String name;
    public final int emptyStands;
    public final int takenStands;
    public final int availableBicycles;

    public StationStatus(String name, int emptyStands, int takenStands, int availableBicycles) {
        this.name = name;
        this.emptyStands = emptyStands;
        this.takenStands = takenStands;
        this.availableBicycles = availableBicycles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StationStatus that = (StationStatus) o;
        return emptyStands == that.emptyStands &&
                takenStands == that.takenStands &&
                availableBicycles == that.availableBicycles &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, emptyStands, takenStands, availableBicycles);
    }
}
