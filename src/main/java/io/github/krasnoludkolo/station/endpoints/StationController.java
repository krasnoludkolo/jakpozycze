package io.github.krasnoludkolo.station.endpoints;

import io.github.krasnoludkolo.rest.ResponseMapper;
import io.github.krasnoludkolo.station.domain.StationFacade;
import io.github.krasnoludkolo.station.domain.api.CreateStationRequest;
import io.github.krasnoludkolo.station.domain.api.EditStationRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.beans.ConstructorProperties;

@RestController
final class StationController {

    private final StationFacade stationFacade;


    StationController(StationFacade stationFacade) {
        this.stationFacade = stationFacade;
    }

    @PostMapping("/station")
    public ResponseEntity createNewStation(@RequestBody CreateStationRequest request){
        return ResponseMapper.map(stationFacade.createStation(request));
    }

    @PutMapping("/station/{oldName}")
    public ResponseEntity editStation(@RequestBody StationNewNameRequest newNameRequest, @PathVariable String oldName){
        var request = new EditStationRequest(oldName,newNameRequest.newName);
        return ResponseMapper.map(stationFacade.editStation(request));
    }

    @DeleteMapping("/station/{name}")
    public ResponseEntity deleteStation(@PathVariable String name){
        return ResponseMapper.map(stationFacade.deleteStation(name));
    }

    @GetMapping("/station")
    public ResponseEntity getAllStatuses(){
        return ResponseEntity.ok(stationFacade.getStationsState());
    }

    static class StationNewNameRequest{

        final String newName;

        @ConstructorProperties({"newName"})
        StationNewNameRequest(String newName) {
            this.newName = newName;
        }
    }
}
