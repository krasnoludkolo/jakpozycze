package io.github.krasnoludkolo.station.endpoints;

import io.github.krasnoludkolo.rest.ResponseMapper;
import io.github.krasnoludkolo.station.domain.StationFacade;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
final class BicycleController {

    private final StationFacade stationFacade;

    BicycleController(StationFacade stationFacade) {
        this.stationFacade = stationFacade;
    }

    @PostMapping("/station/{stationName}/borrow")
    public ResponseEntity borrowBicycle(@PathVariable String stationName){
        return ResponseMapper.map(stationFacade.borrowBike(stationName));
    }

    @PostMapping("/station/{stationName}/return")
    public ResponseEntity returnBicycle(@PathVariable String stationName){
        return ResponseMapper.map(stationFacade.returnBike(stationName));
    }

}
