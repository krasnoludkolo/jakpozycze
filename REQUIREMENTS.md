Stwórz REST'owy microserwis wykorzystujący Spring Boota, który obsługuje sieć stacji rowerowych.

Stacja rowerowa umożliwia wypożyczanie/oddawanie rowerów i składa się z:

	- nazwy

	- zbioru stanowisk

	- zbioru rowerów

Dodatkowo:

	- Stanowisko może być wolne albo zajęte.

	- Na pojedynczej stacji może być jedn ocześnie więcej rowerów niż stanowisk.

	- Stacje można dodawać, usuwać i modyfikować.

Dodatkowe zapytanie o stan wszystkich stacji zwraca listę wszystkich stacji w uproszczonym modelu, składającym się z:

	- nazwy

	- liczby stanowisk wolnych

	- liczby stanowisk zajętych

	- liczby dostępnych rowerów

Liczba rowerów w systemie jest stała. Nie uwzględniaj dodatkowych obiektów jak Użytkownik, Płatność, Przejazd.

